

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Training.Models.Database;

namespace Training.Data;

public class Database : DbContext{
    
    public Database(DbContextOptions<Database> options) : base(options)
    {
        
    }

    public DbSet<Role> Roles { get; set; }
    public DbSet<User> Users { get; set; }


    

}
