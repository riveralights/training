using Training.Models.Base;

namespace Training.Models.Database;

public class Role : ModelBase 
{
    [Required]
    public string? Name { get; set; }
    public ICollection<User>? Users { get; set; }
}