using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Training.Models.Base;

namespace Training.Models.Database;

public class User : ModelBase
{
    [Required]
    public string? Name {get; set;}

    [Required]
    [MaxLength(10)]
    public string? Username {get; set;}

    [Display(Name = "Email address")]
    [Required(ErrorMessage = "The email address is required")]
    [EmailAddress(ErrorMessage = "Invalid Email Address")]
    public string? Email { get; set; }

    [Required]
    public string? Password { get; set; }
    
    [Required]
    public int? RoleId { get; set; } = 3;

    [ValidateNever]
    public Role? Role { get; set; }

}