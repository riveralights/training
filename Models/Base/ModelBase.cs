namespace Training.Models.Base;


public abstract class ModelBase{

    [Key]
    public int Id {get;set;}

    public string? created_by {get;set;} = string.Empty;
    public DateTime created_time {get;set;}

    public string? updated_by {get;set;} = string.Empty;

    public DateTime updated_time {get;set;}

    public string? deleted_by {get;set;} = string.Empty;
    public DateTime? deleted_time {get;set;}

    public ModelBase(){

            this.created_time = DateTime.Now;
            this.updated_time = DateTime.Now;
    }
}