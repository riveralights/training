using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.Rendering;
using Training.Models.Database;

namespace Training.Models.ViewModels;

public class UserManageVM
{
    public User User { get; set; }
    public IEnumerable<SelectListItem> RoleList { get; set; }
}