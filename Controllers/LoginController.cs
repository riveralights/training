using Microsoft.AspNetCore.Mvc;
using Training.Controllers.Base;
using Training.Models.Database;

namespace Training.Controllers;

public class LoginController : AppController
{

    public LoginController(Database context) : base(context)
    {
    }

    public IActionResult Index()
    {
        if(HttpContext.Session.GetString("user_id") != null)
        {
            return Redirect("/Welcome");
        }
        return View();
    }

    [HttpPost]
    public IActionResult Index(string Username, string Password)
    {
       var user = _context.Users.SingleOrDefault(x => x.Username == Username);

       if(user is null)
       {
           ViewBag.msg = "User not found, please register first";
           return View("Index");
       }

       bool isValidPassword = BCrypt.Net.BCrypt.Verify(Password, user.Password);

       if(isValidPassword) {
           HttpContext.Session.SetString("username", Username);
           HttpContext.Session.SetString("user_id", user.Id.ToString());
           HttpContext.Session.SetString("role_id", user.RoleId.ToString());
           return Redirect("/Welcome");
       }
       else
       {
           ViewBag.msg = "This data is not match with our credentials.";
           return View("Index");
       }
    }

    public IActionResult Logout()
    {
        HttpContext.Session.Remove("username");
        HttpContext.Session.Remove("user_id");
        HttpContext.Session.Remove("role_id");
        return Redirect("/");
    }
}