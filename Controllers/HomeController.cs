﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Training.Models;
using Training.Models.Database;

namespace Training.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    protected readonly Database _context;

    public HomeController(ILogger<HomeController> logger, Database context)
    {
        _logger = logger;
        _context = context;
    }

    public IActionResult Index()
    {
        return View();
    }
    public IActionResult Register()
    {
        return View();
    }

    [HttpPost, ActionName("Register")]
    [ValidateAntiForgeryToken]
    public IActionResult RegisterPOST(User obj)
    {
        if(ModelState.IsValid)
        {
            _context.Users.Add(obj);
            _context.SaveChanges();
            return Redirect("/Role");
        }
        return View(obj);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
