using Microsoft.AspNetCore.Mvc;
using Training.Controllers.Base;
using Training.Models.Database;

namespace Training.Controllers;

public class RoleController : AppController
{
    public RoleController(Database context) : base(context)
    {
    }

    public IActionResult Index()
    {
        IEnumerable<Role> roles = _context.Roles.ToList();
        return View(roles);
    }

    public IActionResult Create()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Create(Role role)
    {
        if(ModelState.IsValid)
        {
            _context.Add(role);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
        return View(role);
    }

    public IActionResult Edit(int? id)
    {
        if(id == null || id == 0)
        {
            return NotFound();
        }
        var role = _context.Roles.FirstOrDefault(r => r.Id == id);
        
        if(role is null)
        {
            return NotFound();
        }

        return View(role);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Edit(Role role)
    {
        if (ModelState.IsValid)
        {
            _context.Roles.Update(role);
            _context.SaveChanges();
            TempData["success"] = "Category Updated";
            return RedirectToAction("Index");
        }
        return View(role);
    }
}