using Microsoft.AspNetCore.Mvc;
using Training.Controllers.Base;
using Training.Models.Database;

namespace Training.Controllers;

public class WelcomeController : AppController
{
    public WelcomeController(Database context) : base(context)
    {
    }

    public IActionResult Index()
    {
        if(HttpContext.Session.GetString("username") != null){
            ViewBag.username = HttpContext.Session.GetString("username");
            ViewBag.user_id = HttpContext.Session.GetString("user_id");
            return View();
        }
        else 
        {
            TempData["Nologin"] = "You must login first";
            return Redirect("/Login");
        }

        
    }
}