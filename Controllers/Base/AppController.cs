using Microsoft.AspNetCore.Mvc;

namespace Training.Controllers.Base;

public class AppController : Controller
{
    protected readonly Database _context;

    public AppController(Database context)
    {
        _context = context;
    }

    // protected ActionResult CheckSession()
    // {
    //     if (HttpContext.Session.GetString("user_id") is null)
    //     {
    //         TempData["Nologin"] = "You must login first";
    //         return Redirect("/Login");
    //     }
    // }
}