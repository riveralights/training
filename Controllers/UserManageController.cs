using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Training.Controllers.Base;
using Training.Models.Database;
using Training.Models.ViewModels;

namespace Training.Controllers;

public class UserManage : AppController
{
    public UserManage(Database context) : base(context)
    {
    }

    public IActionResult Index()
    {
        if(HttpContext.Session.GetString("role_id") != "1")
        {
            return Redirect("/Error/ForbidUnauthorize");
        }
        IEnumerable<User> users = _context.Users.Include("Role").ToList();
        return View(users);
    }

    public IActionResult Upsert(int? id)
    {
       var user = _context.Users.Find(id);
       IEnumerable<SelectListItem> RoleList = _context.Roles.ToList().Select(
           r => new SelectListItem
           {
               Text = r.Name,
               Value = r.Id.ToString()
           });

        if(user == null)
        {
            return NotFound();
        }
        ViewBag.RoleList = RoleList;
        return View(user);
    }

    [HttpPost, ActionName("Upsert")]
    [ValidateAntiForgeryToken]
    public IActionResult UpsertPOST(int id, User user)
    {
        var updatedUser = _context.Users.FirstOrDefault(u => u.Id == id);

        if (updatedUser != null)
        {
            updatedUser.RoleId = user.RoleId;

            _context.Users.Update(updatedUser);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
        return View(user);
    }
}