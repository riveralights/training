using Microsoft.AspNetCore.Mvc;
using Training.Controllers.Base;
using Training.Models.Database;

namespace Training.Controllers;

public class ErrorController : AppController
{
    public ErrorController(Database context) : base(context)
    {
    }

    public IActionResult ForbidUnauthorize()
    {
        if(HttpContext.Session.GetString("username") != null){
            return View();
        }
        else 
        {
            TempData["Nologin"] = "You must login first";
            return Redirect("/Login");
        }

        
    }
}