using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using Training.Controllers.Base;
using Training.Models.Database;
using Training.Models.ViewModels;


namespace Training.Controllers;

public class RegisterController : AppController
{
    public RegisterController(Database context) : base(context)
    {
    }

    public IActionResult Index()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Index(User user)
    {
        if(ModelState.IsValid)
        {
            var userFromDB = _context.Users.FirstOrDefault(u => u.Email == user.Email && u.Username == user.Username);

            if(userFromDB != null)
            {
                ViewBag.msg = "This data already registered.";
                return View("Index");
            }
            else{
                if(user.Password != null)
                {
                    user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
                }
                _context.Users.Add(user);
                _context.SaveChanges();
                TempData["AfterRegister"] = "Register Successfull. Please Login";
                return Redirect("/Login");
            }
        }
        return View(user);
    }
}